package ro.mta.se.chat.network.interfaces;

import ro.mta.se.chat.network.SocketListener;
import ro.mta.se.chat.network.SocketSender;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

/**
 * Created by Croitoru Andra on 12/5/2015.
 */
public interface ISocket {
    /**
     * Function accept a new connection
     *
     * @param key the selection key
     * @throws IOException if socket is not found
     */
    void accept(SelectionKey key) throws IOException;

    /**
     * Function reads a data from a connection
     *
     * @param key the selection key
     * @throws IOException if channel is not found
     */
    void read(SelectionKey key) throws IOException;

    /**
     * Function writes data from the connection
     *
     * @param key the selection key
     * @throws IOException if channel is not found
     */
    void write(SelectionKey key) throws IOException;

    /**
     * Function close the channel socket
     *
     * @param ch the channel
     */
    void close(SocketChannel ch);

    /**
     * Convert the object to SimpleConnectionObject
     *
     * @return the SimpleConnection object or null
     */
    SocketSender toSimpleConnection();

    /**
     * Convert the object to ServerSocket
     *
     * @return the ServerSocket object or null
     */
    SocketListener toServerSocket();
}
