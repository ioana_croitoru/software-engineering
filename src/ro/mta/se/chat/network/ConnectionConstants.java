package ro.mta.se.chat.network;

/**
 * Created by Croitoru Andra on 12/27/2015.
 */
public class ConnectionConstants {
    /**
     * The message flag
     */
    public static final int MESSAGE_FLAG = 0x1A;
    /**
     * The change nickname flag
     */
    public static final int CHANGE_NICKNAME = 0x1B;

    /**
     * The port get flag
     */
    public static final int PORT_GET = 0x1C;


}
