package ro.mta.se.chat.network;

import ro.mta.se.chat.network.interfaces.ISocket;
import ro.mta.se.chat.network.listeners.*;
import ro.mta.se.chat.network.interfaces.ISocket;
import ro.mta.se.chat.utils.logging.Logger;

import java.io.IOException;

import java.net.InetAddress;
import java.net.InetSocketAddress;

import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Croitoru Andra on 12/26/2015.
 */
public class SocketSender implements Runnable, ISocket {
    /**
     * The local host address
     */
    private InetAddress hostAddress;
    /**
     * The local port
     */
    private int port;
    /**
     * The selector socket
     */
    private Selector selector;
    /**
     * The sokcet channel
     */
    private SocketChannel channel;
    /**
     * The listener which handle the events
     */
    private ConnectionListeners listener;
    /**
     * The change request list  operations
     */
    private final List changeRequests = new LinkedList();
    /**
     * The pending data
     */
    private final List pendingData = new ArrayList();
    /**
     *
     */
    private boolean connected = false;

    /**
     * The constructor of simple connection
     *
     * @param hostAddress the remote host address
     * @param port        the remote port
     * @param listener    the listener to the callback events
     * @throws IOException if selector could not be loaded
     */
    public SocketSender(InetAddress hostAddress, int port, ConnectionListeners listener) throws IOException {
        this.hostAddress = hostAddress;
        this.port = port;
        this.selector = this.initSelector();
        this.listener = listener;
        this.channel = this.initiateConnection();
    }

    /**
     * This function construct the selector
     *
     * @return the selctor
     * @throws IOException if the selector could not be opened
     */
    private Selector initSelector() throws IOException {
        return SelectorProvider.provider().openSelector();
    }

    /**
     * This initiate the conection
     *
     * @return the channel of comunnication
     * @throws IOException if the connection failed to open
     */
    private SocketChannel initiateConnection() throws IOException {
        SocketChannel ch = SocketChannel.open();
        ch.configureBlocking(false);

        ch.connect(new InetSocketAddress(this.hostAddress, this.port));
        synchronized (this.changeRequests) {
            changeRequests.add(new ChangeRequest(ch, ChangeRequest.REGISTER_FLAG, SelectionKey.OP_CONNECT));
        }
        return ch;
    }

    /**
     * Function that returns the communication channel
     *
     * @return the communication channel
     */
    public SocketChannel getChannel() {
        return channel;
    }

    /**
     * This function runs asyncronuos.It is responsable
     * for reading process packets from network
     */
    public void run() {
        while (true) {
            try {
                synchronized (changeRequests) {
                    Iterator changes = changeRequests.iterator();
                    while (changes.hasNext()) {
                        ChangeRequest change = (ChangeRequest) changes.next();
                        switch (change.type) {
                            case ChangeRequest.CHANGEOPS_FLAG:
                                SelectionKey key = change.socket.keyFor(selector);
                                key.interestOps(change.ops);
                                break;
                            case ChangeRequest.REGISTER_FLAG:
                                change.socket.register(selector, change.ops);
                                break;
                        }
                    }
                    changeRequests.clear();
                }
                selector.select();
                Iterator selectedKeys = selector.selectedKeys().iterator();
                while (selectedKeys.hasNext()) {
                    SelectionKey key = (SelectionKey) selectedKeys.next();
                    selectedKeys.remove();
                    if (!key.isValid())
                        continue;
                    if (key.isConnectable())
                        finishConnection(key);
                    else if (key.isReadable())
                        read(key);
                    else if (key.isWritable())
                        write(key);
                }
            } catch (IOException e) {
                Logger.error("error reading from selection");
            }
        }
    }

    private void finishConnection(SelectionKey key) throws IOException {
        try {
            channel.finishConnect();
        } catch (IOException e) {
            key.cancel();
            return;
        }
        key.interestOps(SelectionKey.OP_WRITE);
        if (listener != null && !listener.handleConnection(channel))
            disconnect();
        else
            connected = true;
    }

    /**
     * This function does'n do anything
     *
     * @param key the selection key unused
     * @throws IOException unused
     */
    @Override
    public void accept(SelectionKey key) throws IOException {

    }

    /**
     * This function reads from the channel
     *
     * @param key the selection key
     * @throws IOException if the read is not posible
     */
    public void read(SelectionKey key) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        int count;
        try {
            count = channel.read(buffer);
        } catch (IOException e) {
            disconnect();
            return;
        }
        buffer.flip();
        if (count == -1 || (listener != null && !listener.handleRead(channel, buffer, count)))
            disconnect();
    }

    /**
     * This function write data to channel
     *
     * @param key the selection key
     * @throws IOException if write could not be possible
     */
    public void write(SelectionKey key) throws IOException {
        int count = 0;
        synchronized (pendingData) {
            while (!pendingData.isEmpty()) {
                ByteBuffer buf = (ByteBuffer) pendingData.get(0);
                channel.write(buf);
                count += buf.capacity() - buf.remaining();
                if (buf.remaining() > 0)
                    break;
                pendingData.remove(0);
            }
            if (pendingData.isEmpty())
                key.interestOps(SelectionKey.OP_READ);
        }

        if (listener != null && !listener.handleWrite(channel, count))
            disconnect();
    }

    /**
     * Function close the channel
     *
     * @param ch the channel
     */
    @Override
    public void close(SocketChannel ch) {
        try {
            ch.close();
        } catch (IOException e) {
            Logger.error("error reading from selection");
        }
    }

    /**
     * Convert to SocketSender object
     *
     * @return the current object
     */
    @Override
    public SocketSender toSimpleConnection() {
        return this;
    }

    /**
     * Convert to ServerSocket object
     *
     * @return null
     */
    @Override
    public SocketListener toServerSocket() {
        return null;
    }

    /**
     * Function sends data into the
     *
     * @param data
     */
    public void send(byte[] data) {
        synchronized (changeRequests) {
            changeRequests.add(new ChangeRequest(channel, ChangeRequest.CHANGEOPS_FLAG, SelectionKey.OP_WRITE));
            synchronized (pendingData) {
                pendingData.add(ByteBuffer.wrap(data));
            }
        }
        selector.wakeup();
    }

    /**
     * Function disconnect the local channel
     */
    public void disconnect() {
        if (listener != null && !listener.handleConnectionClose(channel))
            return;
        try {
            channel.close();
        } catch (IOException e) {
            Logger.error("error reading from selection");
        }
        channel.keyFor(selector).cancel();
        synchronized (changeRequests) {
            changeRequests.clear();
        }
    }
}
