package ro.mta.se.chat.network;

import ro.mta.se.chat.network.listeners.ConnectionListeners;
import ro.mta.se.chat.exceptions.ErrorCreateSocketException;
import ro.mta.se.chat.factory.SocketFactory;
import ro.mta.se.chat.models.MessageModel;
import ro.mta.se.chat.models.UserModel;
import ro.mta.se.chat.singleton.Observer;
import ro.mta.se.chat.singleton.UserManager;
import ro.mta.se.chat.utils.*;
import ro.mta.se.chat.utils.logging.Logger;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Croitoru Andra on 12/26/2015.
 */
public class MyPeer implements ConnectionListeners {
    /**
     * The server socket
     */
    private SocketListener server;

    /**
     * The channek of comunnication
     */
    private SocketChannel channel;

    /**
     * This is the default port
     */
    private int port;

    /**
     * The name of the peer
     */
    private String peerName;

    /**
     * The list of childern
     */
    private final List children = new LinkedList();

    /**
     * The list of connections
     */
    private final List connections = new LinkedList();

    /**
     * The constructor that add a new child
     *
     * @param peer the new child
     */
    public MyPeer(MyPeer peer) {
        server = null;
        if (peer != null)
            children.add(peer);
    }

    /**
     * The constructor of peer connection
     *
     * @param peer the peer
     * @param host the host
     * @param port the port
     * @throws IOException if could not create the peer
     */
    public MyPeer(MyPeer peer, String host, int port) throws IOException {
        this.port = port;
        peerName = host + ":" + Integer.toString(port);
        if (peer != null)
            children.add(peer);
        try {
            server = SocketFactory.getNewSocket(SocketFactory.SOCKET_SERVER, port, "".equals(host) ? null : host, this).toServerSocket();
        } catch (ErrorCreateSocketException e) {
            Logger.error("error creating socket");
            return;
        }
        new Thread(server).start();
    }

    /**
     * This function connects to remote host and port
     *
     * @param host the remote host
     * @param port the remote port
     * @throws IOException if could not create the socket
     */
    public void connect(String host, int port) throws IOException {
        SocketSender conn = null;
        try {
            conn = SocketFactory.getNewSocket(SocketFactory.SOCKET_CLIENT, port, host, this).toSimpleConnection();
        } catch (ErrorCreateSocketException e) {
            Logger.error("error creating socket");
            return;
        }
        connections.add(conn);
        new Thread(conn).start();
    }

    /**
     * This function sends a new message
     *
     * @param message message to be send
     * @param rcpt    the peer name
     */
    public void sendMessage(String message, String rcpt) {
        for (Object obj : children) {
            MyPeer peer = (MyPeer) obj;
            if (peer.peerName.equals(rcpt)) {
                sendMessage(message, peer);
                break;
            }
        }
    }

    /**
     * This functions sends a new message
     *
     * @param message the message
     * @param peer    the peer object
     */
    public void sendMessage(String message, MyPeer peer) {
        int len = message.length();
        if (len == 0)
            return;

        send(peer, mkbuffer((byte) 0x1A, message, len).array());
    }

    /**
     * This function is used to write a string into buffer
     *
     * @param buffer the buffer
     * @param str    the string
     * @param len    the len of the string
     */
    private void putString(ByteBuffer buffer, String str, int len) {
        buffer.putInt(len);
        for (int i = 0; i < len; ++i)
            buffer.putChar(str.charAt(i));
    }

    /**
     * This function gets the string from buffer
     *
     * @param buffer the buffer
     * @return the string format
     */
    private String getString(ByteBuffer buffer) {
        int len = buffer.getInt();
        if (len == 0)
            return null;

        char[] data = new char[len];
        for (int i = 0; i < len; ++i)
            data[i] = buffer.getChar();

        return new String(data);
    }

    /**
     * This function creates a new buffer from given params
     *
     * @param request the request
     * @param str     the string
     * @param len     the len of the string
     * @return
     */
    private ByteBuffer mkbuffer(byte request, String str, int len) {
        ByteBuffer out = ByteBuffer.allocate((len * 2) + 5);
        out.put(request);
        putString(out, str, len);
        return out;
    }

    /**
     * This function sends the listen port to the given peer
     *
     * @param peer the given peer
     */
    private void sendPort(MyPeer peer) {
        ByteBuffer buffer = ByteBuffer.allocate(5);
        buffer.put((byte) ConnectionConstants.PORT_GET);
        buffer.putInt(port);
        SocketSender conn = findConnection(peer.channel);
        if (conn != null)
            conn.send(buffer.array());
        else if (server.hasChannel(peer.channel))
            server.send(peer.channel, buffer.array());
    }

    /**
     * This function send the given data to the peer
     *
     * @param peer the given peer
     * @param data the given data
     */
    private void send(MyPeer peer, byte[] data) {
        for (Object obj : connections)
            ((SocketSender) obj).send(data);
        if (peer == null) {
            for (Object o : children) {
                MyPeer p = (MyPeer) o;
                if (server.hasChannel(p.channel))
                    server.send(p.channel, data);
            }
        } else if (server.hasChannel(peer.channel))
            server.send(peer.channel, data);
    }

    /**
     * This function finds the connection  by channel
     *
     * @param ch the given channel
     * @return the connection
     */
    private SocketSender findConnection(SocketChannel ch) {
        for (Object obj : connections) {
            SocketSender c = (SocketSender) obj;
            if (c.getChannel() == ch)
                return c;
        }

        return null;
    }

    /**
     * This function finds the peer on given channel
     *
     * @param ch the given channel
     * @return the peer
     */
    private MyPeer findPeer(SocketChannel ch) {
        for (Object obj : children) {
            MyPeer peer = (MyPeer) obj;
            if (peer.channel == ch)
                return peer;
        }

        return null;
    }

    /**
     * This function handles the write event
     *
     * @param ch    the channel
     * @param count the count
     * @return true
     */
    public boolean handleWrite(SocketChannel ch, int count) {
        return true;
    }

    /**
     * This function handle the event read
     *
     * @param ch     the socket channel
     * @param buffer the buffer to read
     * @param count  the count
     * @return true if succeed false otherwise
     */
    public boolean handleRead(final SocketChannel ch, ByteBuffer buffer, int count) {
        while (buffer.hasRemaining()) {
            byte request = buffer.get();
            switch (request) {
                case ConnectionConstants.MESSAGE_FLAG: {
                    MyPeer p = findPeer(ch);
                    final String message = getString(buffer);
                    final String sender = p.peerName;
                    UserModel model_user = UserManager.getInstance().getUser(sender);
                    MessageModel model = new MessageModel(message, model_user, true);
                    model_user.addNewMessage(model);
                    Observer.getInstance().notifyReceivedMessage(model);
                    break;
                }
                case ConnectionConstants.CHANGE_NICKNAME: {
                    final String name = getString(buffer);
                    final MyPeer peer = findPeer(ch);
                    if (peer != null) {
                        final String oldName = peer.peerName;
                        peer.peerName = name;
                        try {
                            UserModel model = new UserModel(ch.getRemoteAddress().toString(), peer.port,name);
                            UserManager.getInstance().addUser(model);
                            Observer.getInstance().notifyConnected(model);
                        } catch (IOException e) {
                            Logger.error("Colud not find remote address");
                        }
                    }
                }
                break;
                case ConnectionConstants.PORT_GET: {
                    int port = buffer.getInt();
                    MyPeer peer = findPeer(ch);
                    if (peer != null)
                        peer.port = port;
                    break;
                }
                default:
                    break;
            }
        }
        return true;
    }

    /**
     * This function handle the connection
     *
     * @param ch the socket channel
     * @return true if suceed false otherwise
     */
    public boolean handleConnection(final SocketChannel ch) {
        final MyPeer peer = new MyPeer(this);
        children.add(peer);
        peer.channel = ch;
        peer.port = port;
        send(peer, mkbuffer((byte) ConnectionConstants.CHANGE_NICKNAME, peerName, peerName.length()).array());
        sendPort(peer);
        return true;
    }

    /**
     * This function handle the close event
     *
     * @param ch the channel of communication
     * @return true if suceed false otherwise
     */
    public boolean handleConnectionClose(SocketChannel ch) {
        final MyPeer peer = findPeer(ch);
        if (peer != null && peer.channel == ch) {
            String keyUser = peer.channel.socket().getInetAddress().getHostAddress().toString() + ":" + peer.port;
            UserManager.getInstance().getObserver().notifyDisconnected(keyUser);
        }
        children.remove(peer);
        return true;
    }

}
