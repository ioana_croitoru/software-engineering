package ro.mta.se.chat.network.listeners;

import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * Created by Croitoru Andra on 12/26/2015.
 */
public interface ConnectionListeners
{
    boolean handleWrite(SocketChannel ch, int count);

    boolean handleRead(SocketChannel ch, ByteBuffer buffer, int count);

    boolean handleConnection(SocketChannel ch);

    boolean handleConnectionClose(SocketChannel ch);
}
