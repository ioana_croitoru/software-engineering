package ro.mta.se.chat.network;

import java.nio.channels.SocketChannel;

/**
 * Created by Croitoru Andra on 12/26/2015.
 */
public class ChangeRequest {
    /**
     * The REGISTER FLAG
     */
    public static final int REGISTER_FLAG = 0;
    /**
     * THE CHANGE OPERATION FLAG
     */
    public static final int CHANGEOPS_FLAG = 1;
    /**
     * The socket channel
     */
    public SocketChannel socket;
    public int type;
    public int ops;

    public ChangeRequest(SocketChannel socket, int type, int ops) {
        this.socket = socket;
        this.type = type;
        this.ops = ops;
    }
}
