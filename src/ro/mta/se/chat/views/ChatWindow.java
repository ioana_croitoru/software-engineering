package ro.mta.se.chat.views;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Created by Croitoru Andra on 2/2/2016.
 */
public class ChatWindow extends JFrame {
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField jTextField1;
    private String key;

    private void init() {
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jTextField1 = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();

        setMinimumSize(new java.awt.Dimension(506, 401));
        setPreferredSize(new java.awt.Dimension(506, 431));
        setResizable(false);
        getContentPane().setLayout(null);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel1.setToolTipText("");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(179, 11, 150, 30);

        jTextArea1.setEditable(false);
        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(20, 56, 460, 260);

        jTextField1.setToolTipText("");
        getContentPane().add(jTextField1);
        jTextField1.setBounds(20, 330, 310, 30);

        jButton3.setText("Send");
        getContentPane().add(jButton3);
        jButton3.setBounds(340, 330, 70, 30);

        pack();
    }

    public void addNewMessage(String str) {
        this.jTextArea1.append(this.getKey() + "  " + str + "\n");
    }

    public ChatWindow(String keyUser) {
        this.key = keyUser;
        init();
    }

    public ChatWindow() {
        init();
    }

    public void clearField() {
        this.jTextField1.setText("");
    }

    public String getKey() {
        return this.key;
    }

    public void updateKey(String newKey) {
        this.key = newKey;
        this.jLabel1.setText("Chat with " + newKey);
    }

    public void addListsner(ActionListener listener) {
        this.jButton3.addActionListener(listener);
    }

    public String getMessage() {
        return this.jTextField1.getText();
    }

}
