package ro.mta.se.chat.views;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Created by Croitoru Andra on 2/2/2016.
 */
public class LoginView extends JFrame {
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPasswordField jPasswordField1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;

    private void init() {
        jLabel2 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jPasswordField1 = new javax.swing.JPasswordField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();

        jLabel2.setText("jLabel2");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(102, 0, 102));
        setPreferredSize(new java.awt.Dimension(360, 410));
        setResizable(false);
        getContentPane().setLayout(null);
        getContentPane().add(jPanel1);
        jPanel1.setBounds(519, 0, 272, 10);

        jLabel1.setBackground(new java.awt.Color(255, 51, 51));
        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 51, 51));
        jLabel1.setText("Login");
        jLabel1.setToolTipText("");
        jLabel1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        getContentPane().add(jLabel1);
        jLabel1.setBounds(120, 30, 70, 30);

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel3.setText("Password");
        jLabel3.setToolTipText("");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(10, 120, 110, 17);

        jLabel4.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel4.setText("Port");
        jLabel4.setToolTipText("");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(10, 200, 110, 17);

        jPasswordField1.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jPasswordField1.setText("12345678");
        getContentPane().add(jPasswordField1);
        jPasswordField1.setBounds(130, 120, 160, 20);

        jButton1.setBackground(new java.awt.Color(255, 51, 51));
        jButton1.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jButton1.setText("SignUp");
        jButton1.setToolTipText("");
        jButton1.setBorder(new javax.swing.border.MatteBorder(null));
        getContentPane().add(jButton1);
        jButton1.setBounds(180, 280, 80, 30);

        jButton2.setBackground(new java.awt.Color(255, 0, 0));
        jButton2.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jButton2.setText("Login");
        jButton2.setBorder(new javax.swing.border.MatteBorder(null));
        getContentPane().add(jButton2);
        jButton2.setBounds(60, 280, 80, 30);

        jTextField1.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jTextField1.setToolTipText("");

        getContentPane().add(jTextField1);
        jTextField1.setBounds(130, 200, 160, 20);

        jLabel5.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel5.setText("Username");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(10, 80, 110, 17);

        jTextField2.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jTextField2.setToolTipText("Andra");
        getContentPane().add(jTextField2);
        jTextField2.setText("Andra");
        jTextField2.setBounds(130, 80, 160, 20);

        jLabel6.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel6.setText("IP");
        jLabel6.setToolTipText("");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(10, 160, 110, 20);

        jTextField3.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jTextField3.setToolTipText("127.0.0.1");
        jTextField3.setText("127.0.0.1");

        getContentPane().add(jTextField3);
        jTextField3.setBounds(130, 160, 160, 20);

        pack();
        setLocationRelativeTo(null);
    }

    public LoginView() {
        init();
    }

    public String getUsername() {
        return this.jTextField2.getText();
    }

    public String getIp() {
        return this.jTextField3.getText();
    }

    public String getPort() {
        return this.jTextField1.getText();
    }

    public String getPasswordField() {
        return this.jPasswordField1.getText();
    }

    public void addListener(ActionListener listener) {
        this.jButton1.addActionListener(listener); // signup
        this.jButton2.addActionListener(listener); //login
    }

    public JButton getSignup() {
        return this.jButton1;
    }

    public JButton getLogin() {
        return this.jButton2;
    }

    public void clearAll() {
        this.jTextField1.setText("");
        this.jPasswordField1.setText("");
        this.jTextField2.setText("");
        this.jTextField3.setText("");
    }

    /**
     * This function display a message
     *
     * @param message the message to be displayed
     * @param error   if true then message is an error
     */
    public void displayMessage(String message, boolean error) {
        if (!error)
            JOptionPane.showMessageDialog(this, message, "Message", JOptionPane.INFORMATION_MESSAGE);
        else
            JOptionPane.showMessageDialog(this, message, "Error", JOptionPane.ERROR_MESSAGE);
    }

    public void hideW() {
        this.setVisible(false);
    }
}
