package ro.mta.se.chat.models;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * Created by Croitoru Andra on 11/10/2015.
 * This is the view which stores informations about users
 */
public class UserModel {
    /**
     * The status of the user
     */
    public enum Status {
        CONNECTED, CONNECTING, DISCONNECTED
    }

    /**
     * The inital value of status of the user
     */
    private Status status = Status.DISCONNECTED;

    /**
     * The username of the model
     */
    protected String username;

    /**
     * The ip of the model
     */
    protected String ip;

    /**
     * The port of the model
     */
    protected int port;

    /**
     * The password of the model
     */
    protected String password;

    /**
     * The default constructor of the model
     */
    public UserModel() {
    }

    /**
     * The constructor with 3 params
     *
     * @param ip   the ip of the user
     * @param port the port of the user
     */
    public UserModel(String ip, int port) {
        this.ip = ip;
        this.port = port;

    }

    public UserModel(String ip, int port, String name) {
        this.ip = ip;
        this.port = port;
        this.username = name;

    }


    /**
     * This function sets the ip of the user
     *
     * @param ip the ip of the user
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * This function sets the port value to the user
     *
     * @param port the port value to the user
     */
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * This function sets the username of the user
     *
     * @param username the username of the user
     * @throws NullPointerException if the (@param username) is null
     */
    public void setUsername(String username) {
        if (username == null)
            throw new NullPointerException("The username cannot be null");
        this.username = username;
    }

    /**
     * This function returns the username
     *
     * @return returns the username of the user
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * This function get the port of the user
     *
     * @return the port of the user
     */
    public int getPort() {
        return this.port;
    }

    /**
     * This function returns the ip of the user
     *
     * @return the ip of the user
     */
    public String getIP() {
        return ip;
    }

    /**
     * This function set the password value of the user
     *
     * @param password the given password
     * @throws NullPointerException if the (@param password) is null
     */
    public void setPassword(String password) throws NullPointerException {
        if (password == null)
            throw new NullPointerException("The password cannot be null");
        this.password = password;
    }

    /**
     * This function returns the password of the user
     *
     * @return the password of the user
     */
    public String getPassword() {
        return password;
    }

    /**
     * This function returns the hashCode of the user
     *
     * @return the hashCode of the user
     */
    public int hashCode() {
        return (ip + port).hashCode();
    }

    /**
     * This function check if an user is equal with anotherone
     *
     * @param userModel the source user
     * @return true if equal
     */
    public boolean equals(Object userModel) {
        if (userModel == null)
            return false;
        return this.hashCode() == userModel.hashCode();
    }

    /**
     * This function gets the key of the user
     *
     * @return the key of the user
     */
    public String getKey() {
        return ip + ":" + port;
    }

    /**
     * This function gets the status of the string
     *
     * @return the status of the string
     */
    public Status getStatus() {
        return status;
    }

    /**
     * This function set status of the string
     *
     * @param status set status of string
     */
    public void setStatus(Status status) {
        this.status = status;
    }

    /**
     * This function add a new message to the history view
     *
     * @throws NullPointerException if the (@param incomeMsg) is null
     */
    public void addNewMessage(MessageModel model) throws NullPointerException {

    }

}
