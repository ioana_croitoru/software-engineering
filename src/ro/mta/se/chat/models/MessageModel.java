package ro.mta.se.chat.models;

/**
 * Created by Croitoru Andra on 2/1/2016.
 */
public class MessageModel {

    private String text;

    private UserModel user;

    boolean froMe;

    public MessageModel(String text, UserModel model, boolean from) {
        this.text = text;
        this.user = model;
        this.froMe = from;
    }

    public String getName() {
        return user.getUsername();
    }

    public String getText() {
        return this.text;
    }
}
