package ro.mta.se.chat.controllers;

import ro.mta.se.chat.models.MessageModel;
import ro.mta.se.chat.models.UserModel;
import ro.mta.se.chat.singleton.Observer;
import ro.mta.se.chat.singleton.UserManager;
import ro.mta.se.chat.singleton.adapters.EventsAdapter;
import ro.mta.se.chat.views.ChatWindow;
import ro.mta.se.chat.views.ConnectView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Croitoru Andra on 2/2/2016.
 */
public class ConnectionController implements ActionListener {

    ConnectView connectView;

    ChatWindow chatWindow = new ChatWindow();

    private Observer observer;

    public ConnectionController(ConnectView connectView2) {
        this.connectView = connectView2;
        connectView.hideWindow();
        this.connectView.addBtnConnectListener(this);
        observer = UserManager.getInstance().getObserver();
        observer.addListener(new EventsAdapter() {
            @Override
            public void remoteHostDown() {
                connectView.message("Error the host requested seems to be down", true);
            }
        });

        observer.addListener(new EventsAdapter() {
            @Override
            public void onNewUserConnected(UserModel model) {

            }
        });
        observer.addListener(new EventsAdapter() {
            @Override
            public void onError(String message) {
                super.onError(message);
            }

            @Override
            public void onClosedConnection(String msg) {
                super.onClosedConnection(msg);
            }

            @Override
            public void remoteHostDown() {
                super.remoteHostDown();
            }

            @Override
            public void onNewMessageReceived(MessageModel model) {
                super.onNewMessageReceived(model);
                chatWindow.addNewMessage(model.getText());

            }

            @Override
            public void onNewUserConnected(UserModel model) {
                super.onNewUserConnected(model);
                chatWindow.updateKey(model.getUsername());
                chatWindow.setVisible(true);
            }
        });
        chatWindow.addListsner(new MessageListener());

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        String remoteIp = connectView.getIp();
        int remotePort = Integer.valueOf(connectView.port());
        UserManager.getInstance().connect(remoteIp, remotePort);

    }

    public void setScreen() {
        this.connectView.setVisible(true);
    }

    class MessageListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            String key = chatWindow.getKey();
            String text = chatWindow.getMessage();

            UserManager manager = UserManager.getInstance();
            manager.sendMessage(text, key);
            chatWindow.addNewMessage(text);
            chatWindow.clearField();
        }
    }


}
