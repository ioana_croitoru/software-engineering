package ro.mta.se.chat.controllers;

import ro.mta.se.chat.views.ChatWindow;
import ro.mta.se.chat.views.ConnectView;
import ro.mta.se.chat.views.LoginView;

/**
 * Created by Croitoru Andra on 2/2/2016.
 */
public class AppController {
    ConnectionController connectionController;

    StartUpController startUpController;

    static AppController appController;

    private AppController() {
        connectionController = new ConnectionController(new ConnectView());
        startUpController = new StartUpController(new LoginView());
        ;
    }

    /**
     * This function gets the instance of the appController
     *
     * @return
     */
    public static AppController getInstance() {
        if (appController == null) {
            appController = new AppController();
        }
        return appController;
    }

    public ConnectionController getConnectionController() {
        return this.connectionController;
    }

    public StartUpController getStartUpController() {
        return this.startUpController;
    }

    /**
     * This is the the entry point of the application
     *
     * @param args the arguments of the main function
     */
    public static void main(String[] args) {
        AppController controller = AppController.getInstance();
    }

}
