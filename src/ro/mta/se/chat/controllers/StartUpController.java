package ro.mta.se.chat.controllers;

import ro.mta.se.chat.models.UserModel;
import ro.mta.se.chat.singleton.UserManager;
import ro.mta.se.chat.utils.Constants;
import ro.mta.se.chat.utils.Utils;
import ro.mta.se.chat.views.LoginView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Croitoru Andra on 2/2/2016.
 */
public class StartUpController implements ActionListener {
    private LoginView loginView;

    public StartUpController(LoginView loginView) {
        this.loginView = loginView;
        loginView.addListener(this);
        loginView.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        String username = loginView.getUsername();
        String password = loginView.getPasswordField();
        if (e.getSource().equals(loginView.getSignup())) {
            Utils.createPrivateKey(username, password, Constants.KEY_NAME);
            loginView.clearAll();
            loginView.displayMessage("User created succesfully", false);
        } else {
            String ip = loginView.getIp();
            int port = Integer.valueOf(loginView.getPort());
            if (Utils.checkPassword(username, password) == true) {
                loginView.displayMessage("Acces granted", false);
                loginView.hideW();
                UserModel connected = new UserModel(ip, port, ip + ":" + port);
                UserManager.getInstance().setConnected(connected);
                AppController.getInstance().getConnectionController().setScreen();
            } else {
                loginView.displayMessage("Acces denied", true);
                loginView.clearAll();
            }
        }
    }

}
