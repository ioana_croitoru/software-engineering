package ro.mta.se.chat.exceptions;

/**
 * Created by Croitoru Andra on 10/31/2015.
 * This class throws exception if a socket could not be created
 */
public class ErrorCreateSocketException extends Exception {
    /**
     * The default constructor with no params
     */
    public ErrorCreateSocketException() {
        super("");
    }

    /**
     * The constructor wich format the error message accordingly
     *
     * @param message
     */
    public ErrorCreateSocketException(String message) {
        super("Error creating socket : " + message);
    }
}
