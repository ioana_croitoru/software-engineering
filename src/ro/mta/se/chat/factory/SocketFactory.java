package ro.mta.se.chat.factory;

import ro.mta.se.chat.network.SocketListener;
import ro.mta.se.chat.network.SocketSender;
import ro.mta.se.chat.network.interfaces.ISocket;
import ro.mta.se.chat.network.listeners.ConnectionListeners;
import ro.mta.se.chat.exceptions.ErrorCreateSocketException;
import ro.mta.se.chat.utils.logging.Logger;

import java.io.IOException;
import java.net.InetAddress;

/**
 * Created by Croitoru Andra on 12/5/2015.
 */
public class SocketFactory {
    /**
     * This is the SOCKET_CLIENT flag
     */
    public static final int SOCKET_CLIENT = 1;

    /**
     * This is the SOCKET_SERVER flag
     */

    public static final int SOCKET_SERVER = 2;

    /**
     * This function retruns a ne socket
     *
     * @param type     the type of the socket
     * @param port     the port of the socket
     * @param ip       the ip of the socket
     * @param listener the connection listener object
     * @return a new socket connection
     * @throws ErrorCreateSocketException if the socket could not be created
     */
    public static ISocket getNewSocket(int type, int port, String ip, ConnectionListeners listener) throws ErrorCreateSocketException {
        ISocket connection;
        switch (type) {
            case SOCKET_CLIENT:
                try {
                    connection = new SocketSender(InetAddress.getByName(ip), port, listener);
                } catch (IOException e) {
                    Logger.error("error creating simple connection class");
                    throw new ErrorCreateSocketException("Error creating socket");
                }
                break;
            case SOCKET_SERVER:
                try {
                    connection = new SocketListener(InetAddress.getByName(ip), port, listener);
                } catch (IOException e) {
                    Logger.error("error creating socket connection class");
                    throw new ErrorCreateSocketException("Error creating socket");
                }
                break;
            default: {
                throw new ErrorCreateSocketException("Error creating socket");
            }
        }
        return connection;
    }

}
