package ro.mta.se.chat.singleton.adapters;

import ro.mta.se.chat.models.MessageModel;
import ro.mta.se.chat.models.UserModel;
import ro.mta.se.chat.singleton.events.ErrorListeners;
import ro.mta.se.chat.singleton.events.IListeners;

/**
 * Created by Croitoru Andra  on 2/2/2016.
 */
public abstract class EventsAdapter implements IListeners, ErrorListeners {
    @Override
    public void onError(String message) {

    }

    @Override
    public void onClosedConnection(String msg) {

    }

    @Override
    public void remoteHostDown() {

    }

    @Override
    public void onNewMessageReceived(MessageModel model) {

    }

    @Override
    public void onNewUserConnected(UserModel model) {

    }

}
