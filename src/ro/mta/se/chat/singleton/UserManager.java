package ro.mta.se.chat.singleton;

import ro.mta.se.chat.models.UserModel;
import ro.mta.se.chat.network.MyPeer;
import ro.mta.se.chat.utils.logging.Logger;

import java.io.IOException;
import java.util.LinkedList;

/**
 * Created by  Croitoru Andra on 2/2/2016.
 */
public class UserManager {
    LinkedList<UserModel> connectedUsers;

    UserModel userConnected;

    MyPeer peer;

    private Observer observer;

    private static UserManager instance;

    public static UserManager getInstance() {
        if (instance == null) {
            instance = new UserManager();
        }
        return instance;
    }

    private UserManager() {
        connectedUsers = new LinkedList<UserModel>();
        observer = Observer.getInstance();
    }

    public UserModel getUser(String name) {
        for (UserModel model : connectedUsers) {
            if (model.getUsername().equals(name))
                return model;
        }
        return null;
    }

    public void setConnected(UserModel model) {
        this.userConnected = model;
        try {
            peer = new MyPeer(null, model.getIP(), model.getPort());
        } catch (IOException e) {
            Logger.error("Error creating new peer");
        }
    }

    public void connect(String ip, int port) {
        try {
            peer.connect(ip, port);
        } catch (IOException e) {
            observer.notifyRemoteHostDown();
        }
    }

    public Observer getObserver() {
        return this.observer;
    }

    public void addUser(UserModel model) {
        this.connectedUsers.add(model);
    }

    public void sendMessage(String message, String peerName) {
        peer.sendMessage(message, peerName);
    }
}
