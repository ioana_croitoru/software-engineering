package ro.mta.se.chat.singleton;

import ro.mta.se.chat.models.MessageModel;
import ro.mta.se.chat.models.UserModel;
import ro.mta.se.chat.singleton.events.ErrorListeners;
import ro.mta.se.chat.singleton.events.IListeners;
import ro.mta.se.chat.utils.logging.Logger;

import java.util.Collection;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by Croitoru Andra on 2/2/2016.
 */
public class Observer {
    private static volatile Observer instance = null;

    /**
     * This function contains the listeners that wants to be updated
     * by changes
     */
    private Collection<IListeners> core_listeners = new ConcurrentLinkedQueue<IListeners>();

    /**
     * This function contains the listeners that wants to be updated
     * by changes
     */
    private Collection<ErrorListeners> error_listeners = new ConcurrentLinkedQueue<ErrorListeners>();

    public void addListener(IListeners listeners) {
        this.core_listeners.add(listeners);
    }

    /**
     * This function starts the observer and network manager
     */

    public void start() {

    }

    /**
     * This function stops the observer and network manager
     */
    public void stop() {

    }

    /**
     * This is a function that returns the instance of Observer Manager
     *
     * @return the instance object
     */

    public static Observer getInstance() {
        if (instance == null)
            synchronized (Observer.class) {
                instance = new Observer();
            }
        return instance;
    }

    /**
     * This function notify that an user is trying to connect to us
     *
     * @param model the source user
     */
    public void notifyConnected(final UserModel model) {
        for (final IListeners listener : core_listeners) {
            try {
                listener.onNewUserConnected(model);
            } catch (Throwable cause) {
                Logger.warn("Notifying income connection");
            }
        }
    }

    public void notifyOnError(String cause) {
        for (final ErrorListeners listener : error_listeners) {
            try {
                listener.onError(cause);
            } catch (Throwable th) {
                Logger.warn("Notifying error");
            }
        }
    }

    /**
     * This function notify the listeners that an user has disconnected from us
     *
     * @param msg the source user
     */

    public void notifyDisconnected(String msg) {
        for (ErrorListeners listener : error_listeners) {
            try {
                listener.onClosedConnection(msg);
            } catch (Throwable cause) {
                Logger.warn("Notifying disconnected new user");
            }
        }
    }

    /**
     * This function notify the listeners that a new message has arrived
     *
     * @param message that has arrived
     */
    public void notifyReceivedMessage(final MessageModel message) {
        for (IListeners listener : core_listeners) {
            try {
                listener.onNewMessageReceived(message);
            } catch (Throwable cause) {
                Logger.warn("Notifying on new Message");
            }
        }
    }

    /**
     * This function notify the listeners that a new message has arrived
     */
    public void notifyRemoteHostDown() {
        for (ErrorListeners listener : error_listeners) {
            try {
                listener.remoteHostDown();
            } catch (Throwable cause) {
                Logger.warn("Remoting host down");
            }
        }
    }

    private Observer() {

    }

}
