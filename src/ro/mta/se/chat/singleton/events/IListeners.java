package ro.mta.se.chat.singleton.events;

import ro.mta.se.chat.models.MessageModel;
import ro.mta.se.chat.models.UserModel;

/**
 * Created by Croitoru Andra on 2/2/2016.
 */
public interface IListeners {

    void onNewMessageReceived(MessageModel model);

    void onNewUserConnected(UserModel model);


}
