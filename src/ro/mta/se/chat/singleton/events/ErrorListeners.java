package ro.mta.se.chat.singleton.events;

/**
 * Created by Croitoru Andra on 2/2/2016.
 */
public interface ErrorListeners {

    void onError(String message);

    void onClosedConnection(String msg);

    void remoteHostDown();
}
