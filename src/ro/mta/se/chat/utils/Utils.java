package ro.mta.se.chat.utils;

import ro.mta.se.chat.utils.logging.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.PrivateKey;

/**
 * Created by Croitoru Andra on 2/2/2016.
 */
public class Utils
{
    private static  String keyStore="key_store.jks";
    /**
     * This function creates the private key
     *
     * @param username thhe username of the private key
     * @param password the password of the private key
     */
    public static void createPrivateKey(String username, String password,String keyName) {
        String cmd_process = "cmd.exe";
        String processParam = "/c";
        String preCmd = "start /WAIT ";
        StringBuilder command = new StringBuilder();
        command.append("keytool -genkeypair -alias " + username);
        command.append(" -keyalg RSA -keysize 2048 -validity 360 -keypass " + password);
        command.append(" -storepass " + password);
        command.append(" -keystore " + keyName);
        ProcessBuilder pGenBuilder = new ProcessBuilder(cmd_process, processParam, preCmd + command.toString());
        Process generateProcess = null;
        try {
            generateProcess = pGenBuilder.start();
            generateProcess.waitFor();
        } catch (IOException e) {
            Logger.error("Error generating private key");
        } catch (InterruptedException e) {
            Logger.error("Error generating private key");
        }
    }
    /**
     * Function that check if a username password found a match
     *
     * @param alias    the given username
     * @param password the given password
     * @return true if checking was succesfully false otherwise
     */
    public static boolean checkPassword(String alias, String password) {
        PrivateKey privateKey = null;
        try {
            KeyStore keyStore = KeyStore.getInstance("JKS");
            keyStore.load(new FileInputStream(Utils.keyStore), password.toCharArray());

            KeyStore.PrivateKeyEntry prvKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(alias, new KeyStore.PasswordProtection(password.toCharArray()));
            privateKey = prvKeyEntry.getPrivateKey();
            return true;
        } catch (Exception e) {
            Logger.error("Error loading private key");
            return false;
        }
    }
}
